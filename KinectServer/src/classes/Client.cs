﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class Client {

    byte[] data = null;
    IPEndPoint ipep = null;
    Socket server = null;
    IPEndPoint sender = null;
    EndPoint remote = null;

    public Client () {
        data = new byte[4096];
        //ipep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9050);
        //server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

        IPEndPoint RemoteEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9050);
        Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        string welcome = "Hello, are you there?";
        data = Encoding.ASCII.GetBytes(welcome);
        server.SendTo(data, data.Length, SocketFlags.None, RemoteEndPoint);
    }

    public void ReadMessage () {
        int recv = server.ReceiveFrom(data, ref remote);

        Console.WriteLine(String.Format("Message received from {0}:", remote.ToString()));
        Console.WriteLine(String.Format(Encoding.ASCII.GetString(data, 0, recv)));

    }

    public void CloseServer () {
        Console.WriteLine("Stopping client");
        server.Close();
    }
}