﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class Server {

    IPEndPoint ipep = null;
    Socket newsock = null;
    byte[] data = null;
    IPEndPoint sender = null;
    EndPoint remote = null;

    public Server () {
        data = new byte[4096];
        ipep = new IPEndPoint(IPAddress.Any, 9050);
        newsock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        newsock.Bind(ipep);
        Console.WriteLine("Waiting for a client...");
        sender = new IPEndPoint(IPAddress.Any, 0);
        remote = (EndPoint)(sender);

        //int recv;
        //byte[] data = new byte[1024];
        //IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 9050);

        //Socket newsock = new Socket(AddressFamily.InterNetwork,
        //                SocketType.Dgram, ProtocolType.Udp);

        //newsock.Bind(ipep);
        //Console.WriteLine("Waiting for a client...");

        //IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        //EndPoint Remote = (EndPoint)(sender);

        //Console.WriteLine("Message received from {0}:", Remote.ToString());
        //Console.WriteLine(Encoding.ASCII.GetString(data, 0, recv));

        //string welcome = "Welcome to my test server";
        //data = Encoding.ASCII.GetBytes(welcome);
        //newsock.SendTo(data, data.Length, SocketFlags.None, Remote);
        //while (true)
        //{
        //    data = new byte[1024];
        //    recv = newsock.ReceiveFrom(data, ref Remote);

        //    Console.WriteLine(Encoding.ASCII.GetString(data, 0, recv));
        //    newsock.SendTo(data, recv, SocketFlags.None, Remote);
        //}

    }

    public void ReadMessage() {
        int recv = newsock.ReceiveFrom(data, ref remote);

        Console.WriteLine(String.Format("Message received from {0}:", remote.ToString()));
        Console.WriteLine(String.Format(Encoding.ASCII.GetString(data, 0, recv)));

    }

    public void SendMessage(String message)
    {
        data = Encoding.ASCII.GetBytes(message);
        newsock.SendTo(data, data.Length, SocketFlags.None, remote);
    }

}