﻿using System;
using System.Collections.Generic;

using Vector3D;
using Microsoft.Kinect;

public class Kinect
{
    KinectSensor kinectSensor = null;
    BodyFrameReader bodyFrameReader = null;
    Body[] bodies = null;
    Server server = null;

    public void InitialiseKinect()
    {
        kinectSensor = KinectSensor.GetDefault();
        server = new Server();
        server.ReadMessage();

        if (kinectSensor != null && !kinectSensor.IsOpen)
        {
            kinectSensor.Open();
        }
        bodyFrameReader = kinectSensor.BodyFrameSource.OpenReader();

        if (bodyFrameReader != null)
        {
            bodyFrameReader.FrameArrived += Reader_FrameArrived;
        }
    }

    private void Reader_FrameArrived(object sender, BodyFrameArrivedEventArgs e) {
        bool dataReceived = false;
        String messageToSend = "";
        using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame()) {
            if (bodyFrame != null) {
                if (bodies == null) {
                    bodies = new Body[bodyFrame.BodyCount];
                }
                bodyFrame.GetAndRefreshBodyData(bodies);
                dataReceived = true;

            }
            if (dataReceived) {
                foreach (Body body in bodies) {
                    if (!body.IsTracked) continue;

                    double angle = GetLegsAngle(body);
                    if (angle > 10) {
                        messageToSend = 1 + "#";
                        Console.WriteLine("Angle => " + angle);
                    }
                }
            }
        }
        server.SendMessage(messageToSend);
    }

    double GetLegsAngle (Body body) {
        Joint leftHip = body.Joints[JointType.HipLeft];
        Joint leftKnee = body.Joints[JointType.KneeLeft];
        Joint rightHip = body.Joints[JointType.HipRight];
        Joint rightKnee = body.Joints[JointType.KneeRight];

        Vector leftHipVector = new Vector(leftHip.Position.X, leftHip.Position.Y, leftHip.Position.Z);
        Vector leftKneeVector = new Vector(leftKnee.Position.X, leftKnee.Position.Y, leftKnee.Position.Z);
        Vector rightHipVector = new Vector(rightHip.Position.X, rightHip.Position.Y, rightHip.Position.Z);
        Vector rightKneeVector = new Vector(rightKnee.Position.X, rightKnee.Position.Y, rightKnee.Position.Z);

        return Vector.AngleBetween(Vector.VectorDiff(leftHipVector, leftKneeVector), Vector.VectorDiff(rightHipVector, rightKneeVector));
    }


}