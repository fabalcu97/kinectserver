﻿using System;

namespace KinectServer {
    class Program {
        static void Main(string[] args) {
            var kinect = new Kinect();

            kinect.InitialiseKinect();

            ConsoleKeyInfo info = Console.ReadKey(true);
            do {
                info = Console.ReadKey(true);
            } while (info.Key != ConsoleKey.Escape);
        }
    }
}
